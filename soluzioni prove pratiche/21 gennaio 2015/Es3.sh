#!/usr/bin/bash

#Scrivere un programma python o uno script bash che scandisca il sottoalbero relativo alle directory passate come
#parametri (o alla direcotry corrente se non ci sono parametri) e fornisca in output l'elenco dei file che hanno la
#stessa somma MD5 (i.e. l'output del comando md5sum).
#In output ogni riga deve mostrare un elenco di pathname realtivi a file che hanno la stessa somma MD5 (che quindi
#sono molto molto probabilmente uguali).
#ALGORITMO
#-con find creo in check la lista di tutti i file (ogni riga 1 file) con la propria chiave MD5, che saranno i primi 32 caratteri
#-scorro la lista nel ciclo while considerando sempre la chiave di ogni file
#-con grep prendo tutte le chiavi uguali a quella che sto controllando e se in temp non e` gia` presente lo stesso file copio
# lui e le gli altri file con la stessa chiave in temp e stampo a video
#-NB: ho bisogno di temp perche` altrimenti copio le occorrenze di un file tante volte quante sono le sue occorrenze



if [ $# -eq 1 ]						#Caso in cui passo un path 
then
	path=$1
	find $path -type f -exec md5sum "{}" + > check
else  
	find -type f -exec md5sum "{}" + > check	
fi							

list=check					

touch temp
tmp=temp

while read -r line					
do	
	code=$(cut -c-32 <<< "$line")			#prendo solo i primi 32 caratteri

  	if [ "$(grep -c "$code" "$list")" -gt 1 ]	#se ci sono almeno 2 occorrenze di una chiave 
  	then
		if [ "$(grep -c "$line" "$tmp")" -eq 0 ]	
		then
    			printf "`grep $code $list`\n\n" | tee -a temp	
		fi					
  	fi
done < $list 
rm $tmp				
rm $list						
