/*Prova Pratica di Laboratorio di Sistemi Operativi
21 gennaio 2015
Esercizio 0 (“Se copiate, vi cacciamo”) 
Rendete la vostra directory home inaccessibile ad altri utenti (sia in lettura che in esecuzione). Rimuovete tutti i file 
che vi appartengono dalla directory /public.
Non usare system o popen o simili!
Esercizio 1: Linguaggio C (obbligatorio): (20 punti)
Scrivere un programma C di nome filepipe che abbia come unico parametro il pathnae di un file di testo.
Questo file contiene due comandi con I rispettivi parametri, uno per riga.
Il programma deve mettere in esecuzione concorrente I due comandi in modo che l'output del primo venga fornito
come input del secondo usando una pipe.
Il programma deve terminare quando entrambi I comandi sono terminati.
Esempio: se il file ffff contiene:
ls -l
tac
il comando:
filepipe ffff
deve restituire lo stesso output del comando:
ls -l | tac*/

 #include <stdio.h>
 #include <string.h>
 #include <stdlib.h>
 #include <unistd.h>

 #define CHUNK 1024


int main(int argc, char *argv[]){
	
	int fd[2];
	char line[80];
	char buf[CHUNK];
	char *pch = NULL;
	int iterator = 0;
	char *commands[3];
	char *commands2[3];
	char line1[128];
	char line2[128];

	//Preparo i due array parziali, per i comandi bash 
	commands[0] = "/bin/bash";
	commands[1] = "-c";
	//commands[2] = "ls;";

	commands2[0] = "/bin/bash";
	commands2[1] = "-c";
	//commands2[2] = "cd ..";
	ssize_t nread;
	ssize_t len;
	FILE *in;
	int c=0;
	//leggo il file di testo
	in = fopen(argv[1], "r");
	if(!in){
		printf("Failed to open text file\n");
	}
    //Leggo le due linea, la prima la infilo in line1, la seconda in line2 
	while (iterator <=1 && (nread = getline(&pch, &len, in)) != -1){
		 fwrite(pch, nread, 1, stdout);
		 if (iterator == 0){
		 strcpy(line1, pch);
		} else if (iterator == 1) {
		 strcpy(line2, pch);
		}
		 iterator++;
		}

		//varie stampe di prova
		printf("%s", line1);
		printf("%s", line2);
		
		strcpy(commands[2], line1);
		strcpy(commands2[2], line2);// infilo i comandi letti da file nell'array predisposto a finire della exec
		
		printf("%s", commands[2]);
		printf("%s", commands2[2]);

	commands[3] = NULL;
	commands2[3] = NULL;
	free(pch);
	fclose(in);
	pipe(fd);

	//Procedimento di una pipe
	if(fork()){
		//Processo figlio
		close(1);
		dup(fd[1]);
		close(fd[0]);
		execvp(commands2[0], commands2);
	} else {
		wait(3);
		close(0);
		dup(fd[0]); //Primo processo scrive sulla pip;
		close(fd[1]);
		execvp(commands[0], commands);
	}
exit(EXIT_FAILURE);

return 0;
}