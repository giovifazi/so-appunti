#!/usr/bin/python

import os
import hashlib

#funzione che passato un path prepara una matrice con il nome di ogni file del sottoalbero
#e il suo corrispondente codice md5
def iteratore(list,patty):
        for i,val in enumerate(list):
                stringa = patty + "/" + val
                if (os.path.isdir(stringa)):
                        list2 = os.listdir(stringa)
                        iteratore(list2,stringa)
                else:
                        matrix.append([stringa,hashlib.md5(open(stringa,'rb').read()).hexdigest()])

#stampa a video tutti gli elementi di un array senza le "[ ]"
#p.s qui ho usato il ciclo while perche con il for mi dice che non va bene, non c'ho voglia di capire il perche
def printElements(y):
        print "Questi elementi hanno stessa somma MD5:\n"
        i=0
        while(i<len(y)):
                print y[i]
                i=i+1
        print "__________________________________\n"

#prende la matrice e vede se ci sono elementi con il codice uguale, se si chiama printElements per stamparli
#p.s. anche qui uso i cicli while sempre per i motivi citati sopra (sad vairo is sad)
def findEquals():
        j=0
        while ( j < len(matrix)):
                y = []
                h=0
                y.append(matrix[j][0])
                while (h < len(matrix)):
                        if (matrix[j][1] == matrix[h][1] and matrix[j][0] != matrix[h][0]) and matrix[h][1] !=0:
                                y.append(matrix[h][0])
                                matrix[h][1] = 0 #metto a zero cosi dopo non viene piu calcolato
                        h=h+1
                if(len(y)>1):
                        printElements(y)
                j=j+1

#_______________MAIN______________

path = raw_input("\nPassami un path di una directory:  ")

if os.path.isdir(path):                  #controllo che input sia valido

        if (path == ""):                 #controllo che mi abbiano passato qualcosa, cosi semmai prendo la cwd
                path = os.getcwd()

        list1 =os.listdir(path)          #mi ritorna una lista con i nomi di quello che c'e' sulla directory del path

        matrix =[]                       #matrice vuota che conterra' i nomi di tutti i file con i corrispondenti codici md5

        iteratore(list1,path)

        findEquals()
else:
        print "INPUT NON VALIDO!"
