import sys
import os
import hashlib
import md5

'''Non so se ho fatto nel modo giusto, ma definire global nel main e nel metodo era l'unico modo per farmi funzionare correttamente il programma'''
duplicateValue = 0

def md5reader(fobj):
    """Restituisce il valore md5 del file inviato come parametro"""
    return hashlib.md5(fobj.read()).hexdigest()

def check_for_duplicates(paths, hash=hashlib.sha1):
    global duplicateValue
    hashes = {} 
    for path in paths:
        for dirpath, dirnames, filenames in os.walk(path):
            for filename in filenames:
                full_path = os.path.join(dirpath, filename)
                hashobj = hash()
                for md5file in md5reader(open(full_path, 'rb')):
                    hashobj.update(md5file)
                file_id = (hashobj.digest(), os.path.getsize(full_path))
                duplicate = hashes.get(file_id, None)
                if duplicate:
                    print "Duplicate found: %s and %s" % (full_path, duplicate)
                    duplicateValue+=1
                else:
                    hashes[file_id] = full_path
    

if sys.argv[1:]:
    global duplicateValue
    check_for_duplicates(sys.argv[1:])
    print "%d duplicate founded" % duplicateValue