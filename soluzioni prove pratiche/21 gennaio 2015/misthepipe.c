#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

//errore message with exit
void err(const char msg[])
{
   printf("%s", msg);
   exit(-1);
}

#define BASH "/bin/bash"

int main(int argc, char *argv[]){

   //variables
   pid_t pid;
   FILE *fp;
   char *token, *buffer;
   char first[128], second[128];
   int sz;

   int pfd[2];
   int read_fd;
   int write_fd;

   if(argc != 2)
      err("Invalid number of args");

   //open file and copy content in a buffer
   fp = fopen(argv[1], "r+");
   
   //find end of file, get size, rewind
   fseek(fp, 0, SEEK_END);
   sz = ftell(fp);
   fseek(fp, 0, SEEK_SET);

   //allocate enough space
   buffer = (char *)malloc(sizeof(char)*(sz));
   fread(buffer, 1, sz, fp);
   buffer[sz] = '\0';

   //parse  commands strings
   //first line
   token = strtok(buffer, "\n");
   strcpy(first, token);

   //second line
   token = strtok(NULL, "\n");
   strcpy(second, token);
  
   fclose(fp);

   //pipe, fork and execute with exec 
   pipe(pfd); 
   
   read_fd = pfd[0];
   write_fd = pfd[1];

   pid = fork();
   if(pid == 0)
   {
      //children 
      close(1);
      dup(write_fd);
      close(read_fd);
      execl(BASH, BASH, "-c", first, (char*)NULL);
   }
   else
   {
   	wait(5);
      //parent
      close(0);
      dup(read_fd);
      close(write_fd);
      execl(BASH, BASH, "-c", second, (char *)NULL);

   }
}