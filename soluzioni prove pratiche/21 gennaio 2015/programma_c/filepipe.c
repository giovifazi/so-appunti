#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>


// Algoritmo
// apro il file con fopen
// leggo le righe in delle stringhe dinamicamente allocate
// pipe
// fork
// il padre gira il suo stdout come pipe[1]
// il figlio gira il suo stdin come pipe[0]
// in questo modo il figlio si blocca sulla read quindi non abbiamo problemi di
// concorrenza
// 'gira' = usare la dup
//
// Poi usare system per eseguire i comandi
//
//
// APPUNTI:
// La pipe e` per la comunicazione Unidirezionale da
//
// Filedesc[1] ===> Filedesc[0]
//
//
// In questo es, facendo la fork si creano due processi e vogliamo fare una pipe
// figlio->padre.
//
//Abbiamo:
//       ------------
// stdin-| Figlio   |-stdout->Terminale
//       ------------
//
//       ------------
// stdin-| Padre    |-stdout->Terminale
//       ------------
//
// Filedesc[1] ===> Filedesc[0]
//
// Per reindirizzare figlio.out->padre.in usiamo la dup2.
// Con la dup mettiamo la pipe al posto dello stdin/out; e` importante ricordasi
// di chiudere la 'estremita' della pipe che non interessa nei rispettivi
// processi della fork
//
// cosi succede che
//       ________                            _______
// stdin-|figlio|filedesc[1]====>filedesc[0]-|padre|-stdout->terminale

// Simple error message&quit
void quiterr(const char* errmsg)
{
   printf("%s\n", errmsg);
   exit(EXIT_FAILURE);
}

// Executes cmd1 | cmd2
// Returns -1 on fail, 0 on success
int manual_pipe(char *cmd1, char *cmd2)
{
   // Pipe for comunication
   int fildes[2];

   if ((pipe(fildes)) == -1)
      return -1;

   // Forking
   pid_t pid = fork();

   // Child
   if (pid == 0)
   {
      close(fildes[0]);
      dup2(fildes[1], STDOUT_FILENO);
      system(cmd1);
      exit(0);
   }
   else
   // Parent
   if (pid > 0)
   {
      close(fildes[1]);
      dup2(fildes[0], STDIN_FILENO);
      wait(NULL);    // Parent waits for children to finish
      system(cmd2);
   }
   else
      return -1; 

   return 1;
}

int main(int argc, char **argv)
{
   // Argument check
   if (argc != 2)
      quiterr("Usage: filepipe <path>");

   // File reading
   FILE *file = fopen(argv[1], "r");

   if (file)
   {
      // Line reading stuff
      char *line = NULL;      // Dynamic alloc, must free
      size_t n = 0;
      ssize_t read;
      int i;
      char *cmds[2] = {0};

      // Reading first 2 lines of file
      for (i=0; ((read = getline(&line, &n, file)) != -1) && i<2; i++)
      {
         cmds[i] = (char*) malloc(sizeof(line));
         (*(strchr(line, (int) '\n'))) = '\0';
         strcpy(cmds[i], line);
      }

      // Executing pipe
      if ( ((cmds[0] && cmds[1]) == 0) || (manual_pipe(cmds[0], cmds[1]))== -1)
         quiterr("Pipe error or Wrong commands");

      // Cleaning memory
      free(cmds[0]);
      free(cmds[1]);
      free(line);
   }
   else
      quiterr("Error opening file");
   
   return 0;
}
