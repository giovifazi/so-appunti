#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

#define maxchar 256

int main (int argc, char *argv[])
{
	FILE *fp;
	fp = fopen (argv[1], "r");
	char first_c[maxchar];
	char second_c[maxchar];

	while (fgets(first_c, sizeof first_c, fp) != NULL)      //prendo la prima riga
	{
		break;
	}

	while (fgets(second_c, sizeof second_c, fp) != NULL)    //prendo la seconda riga
	{
		break;
	}
    fclose(fp);

    char *f_ln[4];                                          //preparo l'array di puntatori per la execvp
    f_ln[0] = "/usr/bin/bash";
    f_ln[1] = "-c";
    f_ln[2] = malloc(strlen(first_c) * sizeof(char));
    strcpy(f_ln[2], first_c);
    f_ln[3] = NULL;

	char *s_ln[4];
    s_ln[0] = "/usr/bin/bash";
    s_ln[1] = "-c";
    s_ln[2] = malloc(strlen(second_c) * sizeof(char));
    strcpy(s_ln[2], second_c);
    s_ln[3] = NULL;


    int fd[2];                      
    pid_t child_pid, tpid;
    int child_status;

    if ((pipe(fd)) == -1)                    //creo la pipe
        {
            printf("pipe error\n");  
            exit(0);                            
        }

    if ((child_pid = fork()) == 0)
        {

            close(fd[0]);           
            dup2(fd[1], STDOUT_FILENO);     //collego l'STDOUT del processo alla pipe
    	   	execvp(f_ln[0], f_ln);          //lancio il primo comando
                
        }
    else if (child_pid == -1)
        {
            printf("fork error\n");
            exit(1);
        }
    else 
        {
            close(fd[1]);
       
         do {                               //il processo padre rimane in attesa del figlio
                tpid = wait(&child_status);
            }while(tpid != child_pid);
            

            dup2(fd[0], STDIN_FILENO);      //collego l'STDIN del processo alla pipe
    		execvp(s_ln[0], s_ln);	        //lancio il secondo comando che avra` in input l'output del primo comando grazie
                                            //alla pipe

        }
}























