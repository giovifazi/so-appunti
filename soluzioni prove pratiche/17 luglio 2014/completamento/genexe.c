// Inizio 17:11:40

// Algoritmo
// apri file nella dir corrente
// trova i .o
// if (.c exists) -> check date
// if all .o are more/equally recent as .c
// { compile }
// else
// perror "run genobj first"

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#define CWDLENGHT 2048
#define ARGLENGHT 2048
#define CMDLENGHT 2048

void split_argv(int argc, char *argv[], char *ss)
{
   int i;
   for (i=2; i<argc; i++)
   {
      strcat(ss, argv[i]);
      strcat(ss, " ");
      break;
   }
}


int main(int argc, char *argv[])
{
   if (argc <= 1)
      perror("too few args");

   // Put gcc args into a string
   char gccpara[ARGLENGHT] = {0}, ccommand[CMDLENGHT];
   split_argv(argc, argv, gccpara);

   if (argc > 2)
      sprintf(ccommand, "gcc %s -o %s", gccpara, argv[1]);
   else
      sprintf(ccommand, "gcc -o %s", argv[1]);

   // Look into cwd for .c files
   char cwd[CWDLENGHT];
   DIR *dir;
   struct dirent *file;

   if (getcwd(cwd, sizeof(cwd)) != NULL)
   {
      if ((dir = opendir(cwd)) != NULL)
      {
         while ((file = readdir(dir)) != NULL)
         {
            struct stat srcstat, objstat;
            char *srcpath, *objpath;

            // Check file extension
            if (file->d_name[strlen(file->d_name) - 1] == 'o' &&
                file->d_name[strlen(file->d_name) - 2] == '.'    )
            {
               asprintf(&objpath, "%s/%s", cwd, file->d_name);
               asprintf(&srcpath, "%s/%s", cwd, file->d_name);
               srcpath[strlen(srcpath) - 1] = 'c';

               // Checks if .c file exists
               if ((stat(srcpath, &srcstat) == 0) && (stat(objpath, &objstat) == 0))
               {
                  // Checks if .c was changed after .o
                  if (srcstat.st_mtim.tv_sec > objstat.st_mtim.tv_sec)
                     perror("Run genobj first!\n");
                  else
                  {
                     // add .o to the console command
                     strcat(ccommand, " ");
                     strcat(ccommand, file->d_name);
                     printf("added %s\n", file->d_name);
                  }
               }
               else
                  perror("Run genobj first!\n");

               free(srcpath);
               free(objpath);
            }
         }
         printf("\n%s\n", ccommand);
         system(ccommand);
      }
      else
         perror("opendir() error");
   }
   else
      perror("getcwd() error");

}
