// (re)Inizio 15:43:20

// Algoritmo
// apri file nella dir corrente
// trova i .c
// cerca se esiste il .o corrispondente
// if (.o exists) -> check date
// else gcc -c 

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#define CWDLENGHT 2048
#define ARGLENGHT 2048

void split_argv(int argc, char *argv[], char *ss)
{
   int i;
   for (i=0; i<argc; i++)
      switch (i)
      {
         case 0:
            strcat(ss, "gcc ");
            break;

         default:
            strcat(ss, argv[i]);
            strcat(ss, " ");
            break;
      }

   strcat(ss, "-c ");
}


int main(int argc, char *argv[])
{
   // Put gcc args into a string
   char gccpara[ARGLENGHT] = {0};
   split_argv(argc, argv, gccpara);

   // Look into cwd for .c files
   char cwd[CWDLENGHT];
   DIR *dir;
   struct dirent *file;

   if (getcwd(cwd, sizeof(cwd)) != NULL)
   {
      if ((dir = opendir(cwd)) != NULL)
      {
         while ((file = readdir(dir)) != NULL)
         {
            struct stat srcstat, objstat;
            char *srcpath, *objpath, *ccommand;

            // Check file extension
            if (file->d_name[strlen(file->d_name) - 1] == 'c' &&
                file->d_name[strlen(file->d_name) - 2] == '.'    )
            {
               asprintf(&srcpath, "%s/%s", cwd, file->d_name);
               asprintf(&objpath, "%s/%s", cwd, file->d_name);
               objpath[strlen(objpath) - 1] = 'o';

               // Checks if .o file exists
               if ((stat(objpath, &objstat) == 0) && (stat(srcpath, &srcstat) == 0))
               {
                  // Checks if .c was changed after .o
                  if (srcstat.st_mtim.tv_sec > objstat.st_mtim.tv_sec)
                  {

                     printf(".o Found! Compiling %s\n", file->d_name);
                     asprintf(&ccommand, "%s%s", gccpara, file->d_name);
                     system(ccommand);
                     free(ccommand);
                  }
                  else
                     printf(".o Found but no compilation needed %s\n", file->d_name);
               }
               else
               {
                  printf("No .o found! Compiling %s\n", file->d_name);
                  asprintf(&ccommand, "%s%s", gccpara, file->d_name);
                  system(ccommand);
                  free(ccommand);
               }

               free(srcpath);
               free(objpath);
            }
         }
      }
      else
         perror("opendir() error");
   }
   else
      perror("getcwd() error");

}
