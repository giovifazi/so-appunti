/* 2014-05-29*/
/*PRECONDITION: when you call the program from the command line,
 * insert the path with an ending '/' ex: /home/dir/ */


#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h> //stat
#include<sys/types.h> //stat st
#include<linux/inotify.h>

#define MAX_EVENTS 1024
#define LEN_NAME 16
#define EVENT_SIZE (sizeof(struct inotify_event))
#define BUF_LEN (MAX_EVENTS *(EVENT_SIZE + LEN_NAME))

typedef enum{false, true} bool;

void err(char msg[]){
   printf("%s", msg);
   exit(EXIT_FAILURE);
}
int main(int argc, char *argv[]){

   int fd, wd1, wd2, i, length = 0, wdd;
   char root[256] = "";
   char dir[128] = "directory-base";
   char buffer[BUF_LEN];
   struct stat st = {0};
   bool dir_del;

   /* check if there is just 1 arg */
   if(argc != 2)
      err("Invalid number of args");

   /* if dir doesn't already exist, create it*/ 
   strcpy(root, argv[1]);

   if(root[strlen(root)-1] != '/')
      strcat(root, "/");
   strcat(root, dir);

   if(stat(root, &st) == -1)
   {
      //mode & ~umask & 0777 --> 0644
      if (mkdir(root, 0700) == -1)
         err("Directory creation error"); 
   }
   else
      err("Already existing directory");

   /*create instance of inotify*/
   if((fd = inotify_init()) <0)
      err("inotify_init error");

   /*add directory-base to watch list*/
   /* IN_CREATE, IN_DELETE file/dir created/deleted in the watched dir*/
   if((wd1 = inotify_add_watch(fd, root, IN_CREATE | IN_DELETE | IN_DELETE_SELF) == -1))
      err("watch error");
   else printf("Watching: %s \n", root);
   
   /* add directory-base's parent dir to watch list*/
   if((wd2 = inotify_add_watch(fd, argv[1], IN_DELETE) == -1))
      err("watch error");
   else printf("Watching: %s \n", argv[1]);
    
   /*add '/' to create subdirectories path e.g. .../subdirname */
   strcat(root, "/");
   char tmp[256]="";
   strcpy(tmp, root);

   dir_del = false;
   while(true && !dir_del)
   {
      i = 0;
      
      /*blocking read*/
      if((length = read(fd, buffer, BUF_LEN)) <0)
         err("read error");

      while(i<length)
      {
         struct inotify_event *event = (struct inotify_event *)&buffer[i];

         /* len counts bytes in name, which is present only when an event*/
         /* is returned for a file inside a watched dir*/
         if(event->len)
         {
            if(event->mask & IN_CREATE)
            {
               if(event->mask & IN_ISDIR)
               {
                  strcat(root, event->name);
                  if((wdd = inotify_add_watch(fd, root, IN_CREATE | IN_DELETE | IN_DELETE_SELF) == -1))
                     err("watch error");
                  else {
                     printf("Watching: %s \n", event->name);
                     printf("The dir %s was created. It's %s subdir\n", event->name, tmp);}
               }
               else
                  printf("The file %s was created. \n", event->name);
            }

            if(event->mask & IN_DELETE)
            {
               if(event->mask & IN_ISDIR)
               {
                  /* check exit condition: directory-base deletion*/ 
                  if(strcmp(event->name, dir) == 0)
                  {
                     dir_del = true;
                  }
                  printf("The dir %s was deleted. \n", event->name);
               }
               else
                  printf("The file %s was deleted. \n", event->name);
            }
            if(event->mask & IN_DELETE_SELF)
            {
               dir_del = true;
               printf("directory-base delted. Exit.\n");
            }
         
         }
         /* length of each inotify_event structure*/
         i += EVENT_SIZE + event->len;

         /* reinitialize root with the base-directory path */
         strcpy(root, tmp);
      }
   }

   inotify_rm_watch(fd, wd1);
   inotify_rm_watch(fd, wd2);
   close(fd);

   return 0;
}