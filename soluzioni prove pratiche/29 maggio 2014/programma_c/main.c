#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/inotify.h>

void quiterr(const char *msg)
{
   printf("%s\n", msg);
   exit(-1);
}

int main(int argc, char **argv)
{
   // Controllo parametri
   if (argc != 2)
      quiterr("usage: ./dirlog <path>");

   struct stat bdir;

   if (stat(argv[1], &bdir) == -1)
      mkdir(argv[1], 0700);
   else
      quiterr("Could not create basedir");

   int fd = inotify_init();

   if (fd != -1)
      inotify_add_watch(fd, argv[1], IN_CREATE | IN_DELETE);

   char buffer[8192];
   int length, i=0;

   if ((length = read(fd, &buffer, sizeof(buffer))) > 0)
   {
      while (i<length)
      {
         struct inotify_event *event = (struct inotify_event*) &buffer[i];
         if (event->len)
         {
            if ( event->mask & IN_CREATE) {
            if (event->mask & IN_ISDIR)
              printf( "The directory %s was Created.\n", event->name );      
            else
              printf( "The file %s was Created with WD %d\n", event->name, event->wd );      
          }
           
          if ( event->mask & IN_DELETE) {
            if (event->mask & IN_ISDIR)
              printf( "The directory %s was deleted.\n", event->name );      
            else
              printf( "The file %s was deleted with WD %d\n", event->name, event->wd );      
          } 
 
 
          i += sizeof(struct inotify_event) + event->len;
         }
      }
   }

   return 0;
}
