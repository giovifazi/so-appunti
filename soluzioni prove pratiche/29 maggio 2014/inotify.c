#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>


#define EVENT_SIZE  (sizeof(struct inotify_event))
#define BUF_LEN     (1024 * (EVENT_SIZE + 16))



  static void             /* Display information from inotify_event structure */
 displayInotifyEvent(struct inotify_event *i)
 {
     printf("    wd =%2d; ", i->wd);
     if (i->cookie > 0)
         printf("cookie =%4d; ", i->cookie);
 
     printf("mask = ");
     if (i->mask & IN_ACCESS)        printf("IN_ACCESS ");
     if (i->mask & IN_ATTRIB)        printf("IN_ATTRIB ");
     if (i->mask & IN_CLOSE_NOWRITE) printf("IN_CLOSE_NOWRITE ");
     if (i->mask & IN_CLOSE_WRITE)   printf("IN_CLOSE_WRITE ");
     if (i->mask & IN_CREATE)        printf("IN_CREATE ");
     if (i->mask & IN_DELETE)        printf("IN_DELETE ");
     if (i->mask & IN_DELETE_SELF)   printf("IN_DELETE_SELF ");
     if (i->mask & IN_IGNORED)       printf("IN_IGNORED ");
     if (i->mask & IN_ISDIR)         printf("IN_ISDIR ");
     if (i->mask & IN_MODIFY)        printf("IN_MODIFY ");
     if (i->mask & IN_MOVE_SELF) {    printf("IN_MOVE_SELF "); exit(1);}
     if (i->mask & IN_MOVED_FROM)    printf("IN_MOVED_FROM ");
     if (i->mask & IN_MOVED_TO)      printf("IN_MOVED_TO ");
     if (i->mask & IN_OPEN)          printf("IN_OPEN ");
     if (i->mask & IN_Q_OVERFLOW)    printf("IN_Q_OVERFLOW ");
     if (i->mask & IN_UNMOUNT)       printf("IN_UNMOUNT ");
     printf("\n");
 
     if (i->len > 0)
         printf("        name = %s\n", i->name);
 }

int main(int argc, char *argv[]){
struct stat st = {0};
int inotifyFd, wd;
ssize_t numRead;
char *p;
struct inotify_event *event;
char buf[BUF_LEN];
char dir[128] = "directory-base";
char path[256] = "";
strcpy(path, argv[1]);
strcat(path,dir);
printf(path);

if (stat(path, &st) == -1)
    {
      printf("Creando una nuova cartella...\n");
      if(mkdir(path, 0700) ==-1) 
          { 
 	    printf("Erroraccio\n");
 	    exit(1);
 	 }
 	printf("Creazione riuscita!\n");
} else   {
	printf("Cartella gia' esistente\n");
	}
	
inotifyFd = inotify_init();
if(inotifyFd == -1)
    {
     printf("Errore nella inotify_init()");
     exit(1);
    }
    
wd = inotify_add_watch(inotifyFd, path, IN_ALL_EVENTS);

if(wd == -1)
    {
     printf("Errore sulla notify and watch()");
    }

for(;;)
    {
     numRead = read(inotifyFd, buf, BUF_LEN);
     if (numRead == 0)
         {
             printf("read() from inotify fd returned 0!");
             exit(1);
         }
     if (numRead == -1)
         {
             printf("read error");
             exit(1);
         }
         
          for (p = buf; p < buf + numRead; ) {
             event = (struct inotify_event *) p;
             displayInotifyEvent(event);
 
             p += sizeof(struct inotify_event) + event->len;
         }
    }


return 0;
}