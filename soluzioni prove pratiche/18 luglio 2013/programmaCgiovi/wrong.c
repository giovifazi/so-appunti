#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>

// Algoritmo
// leggi riga per riga l input e salvalo da qualche parte (realloc alloc...)
// quando arriva la riga vuota
// ERRORE IMPORTANTE: Non puoi usare i threads perche` quando chiami execl 
// crei un nuovo processo, per ovviare al problema qui ho usato system che
// pero` non e` permessa. Quindi devi per forza fare delle fork che creano
// processi separati


void quiterr(const char *errmsg)
{
   printf("%s\n", errmsg);
   exit(EXIT_FAILURE);
}

void *dostuff(void *arg)
{
   system((char*) arg);
   printf("Arrivato: %s\n", (char*) arg);

   return NULL;
}

int main(int argc, char **argv)
{
   if (argc != 1)
      quiterr("Usage ./prorace");

   char **cmds = NULL; // realloc will do the work
   char *line = NULL;  // this way getline uses dyn alloc
   size_t size = 0;
   int i=0;            // lines entered

   // Reading stdin
   do
   {
      if (line)
         free(line);

      size = 0;

      if (getline(&line, &size, stdin) != -1)
      {
         cmds = realloc(cmds, sizeof(char*) * (i+1));
         cmds[i] = (char*) malloc(sizeof(*line));
         *strchr(line, (int) '\n') = '\0';
         strcpy(cmds[i], line);
         i++;
      }
   } while (strcmp(line, "\0") != 0);

   if (line)
      free(line);

   if (i == 1)
      quiterr("Not enough commands");

   // launching threads
   pthread_t threads[i-1];
   int k;

   for (k=0; k<i-1; k++)
      if ((pthread_create(&threads[k], NULL, &dostuff, (void*) cmds[k])) != 0)
         quiterr("Could not create threads");
      
   for (k=0; k<i-1; k++)
      pthread_join(threads[k], NULL);

   return 0;
}

