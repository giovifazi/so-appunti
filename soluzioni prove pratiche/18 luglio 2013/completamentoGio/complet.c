#define _GNU_SOURCE
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <poll.h>

// Algoritmo
// leggi riga per riga l input e salvalo da qualche parte (realloc alloc...)
// quando arriva la riga vuota
// ERRORE IMPORTANTE: Non puoi usare i threads perche` quando chiami execl 
// crei un nuovo processo, per ovviare al problema qui ho usato system che
// pero` non e` permessa. Quindi devi per forza fare delle fork che creano
// processi separati

// Completamento
// creo una pipe e faccio poll per aspettare che si scriva sulla pipe
// Quando il padre scrive sulla pipe fa broadcast ai figli che partono


void quiterr(const char *errmsg)
{
   printf("%s\n", errmsg);
   exit(EXIT_FAILURE);
}

void childexec(char* cmd)
{
   char *para = cmd;
   cmd = strsep(&para, " ");
   char *fullpath;

   asprintf(&fullpath, "/bin/%s", cmd);
   
   execl(fullpath, fullpath, para, (char*) 0);

   free(fullpath);
}

int findex (pid_t p, pid_t arr[], int max)
{
   int ret;
   for (ret=0; ret<max; ret++)
      if (p == arr[ret])
         return ret;

   return -1;
}

int main(int argc, char **argv)
{
   if (argc != 1)
      quiterr("Usage ./prorace");

   char **cmds = NULL; // realloc will do the work
   char *line = NULL;  // this way getline uses dyn alloc
   size_t size = 0;
   int i=0;            // lines entered

   // Reading stdin
   do
   {
      if (line)
         free(line);

      size = 0;

      if (getline(&line, &size, stdin) != -1)
      {
         cmds = realloc(cmds, sizeof(char*) * (i+1));
         cmds[i] = (char*) malloc(sizeof(*line));
         *strchr(line, (int) '\n') = '\0';
         strcpy(cmds[i], line);
         i++;
      }
   } while (strcmp(line, "\0") != 0);

   if (line)
      free(line);

   if (i == 1)
      quiterr("Not enough commands");

   // Preparing pipe. 1 write, 0 read
   int pstart[2];

   if ((pipe(pstart)) == -1)
      quiterr("Pipe Error");

   // Preparing poll
   struct pollfd fds;
   fds.fd = pstart[0];
   fds.events = POLL_IN;

   // Launching forks
   pid_t pid[i], cpid;
   int k, terminated = 0, status;

   for (k=0; k<(i-1); k++)
   {
      switch((pid[k] = fork()))
      {
         // Children
         case 0:
            if ((poll(&fds, 1, -1)) == POLL_IN)
            {
               childexec(cmds[k]);
               exit(0);
            }
            else
               quiterr("Poll error");
         break;

         // Bad fork
         case -1:
            quiterr("Fork error");
         break;
      }
   }

   char *startsig = "partite figli miei";
   write(pstart[1], startsig, sizeof(startsig));

   // Parent
   do
   {
      switch (cpid = waitpid(-1, &status, WNOHANG))
      {
         case 0:
            break;

         case -1:
            quiterr("Running process error");
            break;

         default:
            terminated++;
            printf("%d Arrivato: %s\n", terminated, cmds[findex(cpid, pid, i-1)]);
      }
   }
   while (terminated < i-1);

   return 0;
}

