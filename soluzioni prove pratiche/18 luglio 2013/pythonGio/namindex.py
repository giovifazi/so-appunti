#!/bin/env python3

import os
import sys

def noDoubleAdd( df, name, value, suffix ):
    "Checks if name is a key of df, if it is a double adds a suffix and cheks again"
    # caso dizionario vuoto
    if (not df):
        df[name] = value
        return

    # controllo doppi
    for key in list(df):
        if (name == key):
            noDoubleAdd( df, str(name) + str(suffix), value, int(suffix+1) )
            return
        else:
            df[name] = value
            return

dict_files = {}

for root, dirs, files in os.walk(sys.argv[1], topdown=True):
    for name in files:
        noDoubleAdd(dict_files, name, os.path.join(root, name), 1)

# creates simbolic links
for keys in dict_files:
    os.symlink(dict_files[keys], sys.argv[2] + keys)
