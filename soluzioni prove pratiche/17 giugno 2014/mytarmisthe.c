#include <stdio.h>
#include <sys/types.h> 
#include <dirent.h>
#include <stdlib.h> //exit
#include <sys/stat.h> //stat

void err(const char msg[])
{
   printf("%s", msg);
   exit(-1);
}

FILE *openfile( const char *dirname, struct dirent *dir, const char *mode)
{
   char pathname[1024];
   FILE* fp;

   sprintf(pathname, "%s%s", dirname, dir->d_name);
   fp = fopen(pathname, mode);
   return fp;
}

int count(FILE *fp)
{
   char ch;
   int i = 0;
   while((ch = fgetc(fp)) != EOF)
      i++;
   return i;
}

int main(int argc, char *argv[]){

   char pat[1024];
   long int size;
   int nc = 0;
   char *con;
   struct stat st;

   FILE *fp;
   FILE *fpd;

   DIR *dp;
   struct dirent *ep;

   if(argc != 3)
      err("Invalid number of arguments");
   else
   {
      dp = opendir(argv[1]);
      if (dp == NULL)
         err("Couldn't open the directory");
      
      fp = fopen(argv[2], "w+");
      if(fp == NULL)
         err("Couldn't open the file");

      while (ep = readdir(dp))
         if(ep->d_type == DT_REG)
         {
            fpd = openfile(argv[1], ep, "r+");
            
            //get file size
            sprintf(pat, "%s%s", argv[1], ep->d_name);
            stat(pat, &st);
            size = (long int)st.st_size;

            //create content buffer
            nc = count(fpd);
            rewind(fpd);
            con = (char *)malloc(sizeof(char)*(nc+1));
            con[nc] = '\0';

            //write file info and content
            fread(con, sizeof(char), nc, fpd);            
            fprintf(fp, "%s %ld %s \n", ep->d_name, size, con);

            free(con);
            fclose(fpd);
          }
      fclose(fp);
   }
}
