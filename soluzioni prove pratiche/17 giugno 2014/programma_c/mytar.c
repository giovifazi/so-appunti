#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>

#define FSIZE 4096   // This must be a power of 2

// Algoritmo
// Scorri file regolari nella directory
// Nel main apri il file argv[2]
// con una funzione fai la write dentro il file aperto
//

// Prints error message and quits with -1 status
void quiterr(const char *msg)
{
   printf("%s\n", msg);
   exit(-1);
}

int main(int argc, char**argv)
{
   if (argc != 3)
      quiterr("Usage: mytar <directory path> filename.mytar");

   // Directory opening
   DIR *dir = opendir(argv[1]); 
   struct dirent *entry;

   if (dir != NULL)
   {
      // Creates tar-like file
      FILE *tfile = fopen(argv[2], "w"), *tmpfile; 

      if (tfile == NULL)
         quiterr("Could not create tar file");

      // Readdir returns Null on stream end or fail
      // so we check failures with errno
      errno = 0;  

      while ((entry = readdir(dir)) != NULL)
      {
         if (entry->d_type == DT_REG)
         {
            char *fname;
            asprintf(&fname, "%s/%s", argv[1], entry->d_name);
            tmpfile = fopen(fname, "r");

            fseek(tmpfile, 0, SEEK_END);
            int fsize = ftell(tmpfile);
            char *str_fsize;
            asprintf(&str_fsize, "%d%c", fsize, '\0');
            rewind(tmpfile);

            char *content = (char*) malloc(sizeof(char) * (fsize + 1));
            content[fsize] = '\0';

            fread(content, 1, fsize, tmpfile);
            fprintf(tfile, "%s%c%s%c%s%c", &fname[strlen(argv[1])+1], '\0', str_fsize,'\0', content, '\0');

            free(fname);
            free(str_fsize);
            fclose(tmpfile);
         }
      }
      
      fclose(tfile);

      if (errno != 0)
         quiterr("Error while reading dir");
   }
   else
      quiterr("Error trying to open dir!");

   return 0;
}
