#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>

// Algoritmo

void detar(char *str, int start, int end, char *dirname)
{
   char *name, *size, *cont;

   if (start == 0)
   {
      name = &(str[start]);
      size = &(str[start + strlen(name) + 1]);
      cont = &(str[start + strlen(name) + strlen(size) + 2]);
   }
   else
   {
      name = &(str[start+1]);
      size = &(str[start + strlen(name) + 2]);
      cont = &(str[start + strlen(name) + strlen(size) + 3]);
   }

   FILE *file;
   char *fullpath;

   asprintf(&fullpath, "%s/%s", dirname, name);

   file = fopen(fullpath, "w");

   fwrite(cont, sizeof(char), atoi(size), file);
   fclose(file);

   free(fullpath);
}

// Prints error message and quits with -1 status
void quiterr(const char *msg)
{
   printf("%s\n", msg);
   exit(-1);
}

int main(int argc, char**argv)
{
   if (argc != 3)
      quiterr("Usage: myuntar filename.mytar <directory>");

   // Creates directory
   mkdir(argv[2], 777);

   char *tarcont;
   FILE *tar = fopen(argv[1], "r");
   int tsize, i = 0;

   if (tar != NULL)
   {
      fseek(tar, 0, SEEK_END);
      tsize = ftell(tar);
      tarcont = (char*) malloc(tsize);
      rewind(tar);
      
      while (i < tsize)
      {
         tarcont[i] = (unsigned char) fgetc(tar);
         i++;
      }

      fclose(tar);
   }

   int count = 0, start = 0;

   for (i=0; i<tsize; i++)
   {
      if (tarcont[i] == '\0')
      {
         count++;

         if (count == 3)
         {
            detar(tarcont, start, i, argv[2]);
            start = i;
            count = 0;
         }
      }
   }

   return 0;
}
