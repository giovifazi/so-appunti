#!/usr/bin/python3

import os
import sys
import collections

def FreqOf(filepath, letter):
    """Opens file, reads into a string and excludes \n, then counts the
    occurrences of letter and returns the frequency in percentage"""
    with open(filepath, 'r') as myfile:
        data = myfile.read().replace('\n', '')
    letters = collections.Counter(data)
    return (letters[letter] * 100 / len(data))


dictNameFreq = {}

for root, dirs, files in os.walk(sys.argv[2], topdown=True):
    for name in files:
        dictNameFreq[str(name)] = FreqOf(os.path.join(root, name), sys.argv[1])

maxFreq = max(dictNameFreq, key=dictNameFreq.get)
print(maxFreq, dictNameFreq[maxFreq])
