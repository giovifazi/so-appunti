
'''Per ora lavoro su una cartella specifica che contiene dei miofile.n'''
os.chdir('/home/students/diego.deangelis/Desktop')


'''La versione ricorsiva e' quella che preferisco perche' mi fa sentire pro'''
def creation(file_name, file_version):
    if file_version == 2:
        return 0
    else:
        for file in glob.glob('{}.{}'.format(file_name, file_version - 1)):
            new_file = '{}.{}'.format(file_name, file_version)
            os.rename(file, new_file)
            print(file)
        return creation(file_name, file_version-1)   



'''Il check cerca tra miofile.3 e miofile.1 l'uguaglianza dei contenuti. Ritorna 0 o 1'''
def check(file_name, file_version):
    for file in glob.glob('{}.{}'.format(file_name, file_version)):
        file1 = '{}.{}'.format(file_name, '3')
        file2 = '{}.{}'.format(file_name, '1')        
    return filecmp.cmp(file1,file2)



'''Crea il link tra il file miofile.3 e miofile.1'''
def createLink(file_name, file_version):
    for file in glob.glob('{}.{}'.format(file_name, file_version)):
        file1 = '{}.{}'.format(file_name, '2')
        file2 = '{}'.format(file_name)       
        os.link(file1, file2)
        
        
'''Copyfile fa la copia del contenuto dimiofile in miofile.1'''
def copyfile(file_name, file_version):
    for file in glob.glob('{}.{}'.format(file_name, file_version)):
        file1 = '{}'.format(file_name)
        file2 = '{}.{}'.format(file_name, '1')
        copyfile(file1, file2)   



'''IMPORTANTE: creare miofile.n di esempio, da miofile, miofile.1 a miofile.n e nel terminale chiamare il backup miofile n+1'''


'''prendo come argomento miofile(file_name) n(file_version)'''
if sys.argv[2:]:
    file_name = sys.argv[1]
    global file_version
    file_version = int(sys.argv[2])
'''Stampe di prova'''
print(file_name)
print(file_version)
creation(file_name, file_version)
if check(file_name, file_version):
    print("Sono uguali")
    createLink(file_name, file_version)
    print("Eseguito un hard link :))))")
else:
    print("Diversi")
copyfile(file_name, file_version)
