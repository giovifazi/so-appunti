#!/usr/bin/env python3

import os
import sys
import subprocess
import magic

m=magic.open(magic.MAGIC_NONE)
m.load()

searchtype = m.file(sys.argv[1])
print(searchtype)

for root, dirs, files in os.walk(sys.argv[2], topdown=True):
    for name in files:
        fullpath = os.path.join(root, name)
        ctype = m.file(fullpath)

        if (searchtype == ctype):
            print("{} matches".format(fullpath))

