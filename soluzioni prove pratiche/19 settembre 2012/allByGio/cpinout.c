#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>

// Number of bytes written
int Bwritten = 0;
double avg = 0;

// Clock for execution time
clock_t begin, end;

void quiterr(const char *errmsg)
{
   printf("%s\n", errmsg);
   exit(EXIT_FAILURE);
}

void sighandler(int signum)
{
   switch (signum)
   {
      case SIGUSR1:
         end = clock();
         avg = ((double) Bwritten) / ((double) (end - begin) / CLOCKS_PER_SEC);
         fprintf(stderr, "\nCopied %d Bytes\nAverage: %f Bps\n", Bwritten, avg);
         quiterr("");
         break;

      case SIGALRM:
         fprintf(stderr, "\nCopied %d Bytes\n", Bwritten);
         break;
   }
}

int main(int argc, char **argv)
{
   begin = clock();

   // Preparing timer for SIGALARM
   struct itimerval timer;

   signal(SIGALRM, sighandler);
   timer.it_value.tv_sec = 1000;
   timer.it_interval.tv_sec = 1000;

   if (setitimer(ITIMER_PROF, &timer, NULL) != -1)
      quiterr("Could not set timer");

   // Sets the handler for SIGUSR1
   signal(SIGUSR1, sighandler);

   char buffer, ldchar[4] = {'-', '\\', '|', '/'};
   int ldind = 0;

   while (1)
   {
      if ((read(STDIN_FILENO, &buffer, 1)) == 1)
      {
         // Read & write
         fprintf(stdout, "%c", buffer);
         Bwritten++;

         // Animation loop
         fprintf(stderr, "\r%c", ldchar[ldind % 4]);
         ldind++;
      }
   }


   return 0;
}
