#!/bin/bash

# Checking files with same name
for file1 in $1/*; do
   isdiff=0
   for file2 in $2/*; do
      # substring of file1: excluding length of first dir name
      if [ ${file1:${#1}} = ${file2:${#2}} ]
      then
         cmp ${file1} ${file2}  
         if [ $? -ne 0 ]
         then
            cp ${file1} ${2}
            echo "copied ${file1} ${2}"
            break
         else
            isdiff=1
            echo "${file1} is the same ${file2}"
         fi
      fi
   done

   if [ $isdiff -eq 0 ]
   then
      cp ${file1} ${2}
      echo "copied ${file1} ${2}"
   fi
done
